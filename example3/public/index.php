<?php

require __DIR__ . '/../vendor/autoload.php';

use App\EchoService;

echo 'Test is a sample php file for test: ' . (new EchoService())->say('test');
